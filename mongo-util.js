import { MongoClient } from 'mongodb';

let db_instance;
let gem_mobile_db_url = "mongodb://localhost:27017/"

export const connectToServer = (callback) => {
	MongoClient.connect(gem_mobile_db_url, { useNewUrlParser: true }, (err, client) => {
		if (err)
			client.close();
		else db_instance = client.db('gem_tutor');
		return callback(err);
	})
};

export const getDbInstance = () => { return db_instance; };