import express from 'express';
import passport from 'passport';
import FacebookStrategy from 'passport-facebook';
import GoogleStrategy from 'passport-google-oauth20';
// Import Facebook and Google OAuth apps configs
import { facebook, google } from './config';
import * as GemDb from './mongo-util'
import GemRouter from './routes'

import https from 'https';
import fs from 'fs';

// Transform Facebook profile because Facebook and Google profile objects look different and we want to transform them into user objects that have the same set of attributes
const transformFacebookProfile = (profile) => ({
	name: profile.name,
	avatar: profile.picture.data.url,
	email: profile.email
});

// Transform Google profile into user object
const transformGoogleProfile = (profile) => ({
	name: profile.displayName,
	avatar: profile.image.url,
});

// Register Facebook Passport strategy
passport.use(new FacebookStrategy(facebook,
  	// Gets called when user authorizes access to their profile
  	async (accessToken, refreshToken, profile, done) => done(null, transformFacebookProfile(profile._json))
  	));

// Register Google Passport strategy
passport.use(new GoogleStrategy(google,
  	async (accessToken, refreshToken, profile, done) => done(null, transformGoogleProfile(profile._json))
  	));

// Serialize user into the sessions
passport.serializeUser((user, done) => done(null, user));

// Deserialize user from the sessions
passport.deserializeUser((user, done) => done(null, user));





/*---------- Initialize http server ----------*/
var options = {
	key: fs.readFileSync('privateKey.key'),
	cert: fs.readFileSync('certificate.crt')
};

const app = express();

GemDb.connectToServer((err) => {
	if (err) {
		console.log("[ERROR] Failed to connecto to MongoDB: ", err);
		return;
	}

	// Initialize Passer Request
	app.use(express.json());       // to support JSON-encoded bodies
	app.use(express.urlencoded({extended: true})); // to support URL-encoded bodies

	// Initialize Passport
	app.use(passport.initialize());
	app.use(passport.session());

	app.get('/', function(req,res) {
	  	res.send('Test Index Page');
	});

	// Set up Facebook auth routes
	app.get('/auth/facebook', passport.authenticate('facebook'));

	app.get('/auth/facebook/callback',
	  	passport.authenticate('facebook', { failureRedirect: '/auth/facebook' }),
	  	// Redirect user back to the mobile app using Linking with a custom protocol OAuthLogin
	  	(req, res) => res.redirect('OAuthLogin://login?user=' + JSON.stringify(req.user)));

	// Set up Google auth routes
	app.get('/auth/google', passport.authenticate('google', { scope: ['profile'] }));

	app.get('/auth/google/callback',
	  	passport.authenticate('google', { failureRedirect: '/auth/google' }),
	  	(req, res) => res.redirect('OAuthLogin://login?user=' + JSON.stringify(req.user)));

	
	GemRouter.setRoute(app);


	// Launch the server on the port 3000
	const server = app.listen(5000, () => {
	  	const { address, port } = server.address();
	  	console.log(`HTTP://${address}:${port}`);
	});


	var https_server = https.createServer(options, app);
	https_server.listen(5443, function(){
	  	console.log("HTTPS")
	});
});