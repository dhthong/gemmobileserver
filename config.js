export const facebook = {
  clientID: '225703488153202',
  clientSecret: 'a91262ed70e94bdb7c8994ed431221c9',
  callbackURL: 'https://192.168.10.41:3001/auth/facebook/callback',
  profileFields: ['id', 'name', 'displayName', 'picture', 'email'],
};

export const google = {
  clientID: 'INSERT-CLIENT-ID-HERE',
  clientSecret: 'INSERT-CLIENT-SECRET-HERE',
  callbackURL: 'http://localhost:3000/auth/google/callback',
};