import * as GemDb from './mongo-util'
import nodemailer from 'nodemailer';

const transport = nodemailer.createTransport({
    service: 'Gmail',
    auth: {
        user: 'snowfox288@gmail.com',
        pass: '12#32!0aA',
    },
});

const getGradeById = (id) => {
    switch (id) {
        case 1: return 'Lớp 1';
        case 2: return 'Lớp 2';
        case 3: return 'Lớp 3';
        case 4: return 'Lớp 4';
        case 5: return 'Lớp 5';
        case 6: return 'Lớp 6';
        case 7: return 'Lớp 7';
        case 8: return 'Lớp 8';
        case 9: return 'Lớp 9';
        case 10: return 'Lớp 10';
        case 11: return 'Lớp 11';
        case 12: return 'Lớp 12';
        case 13: return 'Cấp 1';
        case 14: return 'Cấp 2';
        case 15: return 'Cấp 3';
        default: return '';
    }
}


exports.createTutorRequest = (req, res) => {	
	let db = GemDb.getDbInstance();
	let tutor_req = req.body.tutor_req;

    let tutor_mode = tutor_req.mode ? 'Gia sư online' : 'Gia sư tại nhà';
    let tutor_subject = '';

    if (tutor_req.subjects.length > 0)
    {
        tutor_subject += tutor_req.subjects[0];
        for (let i = 1; i < tutor_req.subjects.length; i++)
            tutor_subject += (' – ' + tutor_req.subjects[i]);
    }

    let mailContent = '<p>Loại: ' + tutor_mode + '</p>' + '<p>Cấp học: ' + getGradeById(tutor_req.grade) + '</p>'
    + '<p>Môn học: ' + tutor_subject + '</p>' + '<p><b>Điện thoại liên hệ: </b>' + tutor_req.user_phone + '</p>' + '<p><b>Email: </b>' + tutor_req.user_email + '</p>';

    let mailOptions = {
        from: 'snowfox288@gmail.com', // sender address
        to: 'dhthong.hcmus@gmail.com', // list of receivers
        subject: '[GEM Tutor] ' + tutor_req.user_email, // Subject line
        html: mailContent // html body
    };
	// tutor_req.type = req.body.type;
	// tutor_req.name = req.body.name;
	// tutor_req.email = req.body.email;
	// tutor_req.subjects = req.body.subjects;
	// tutor_req.grade = req.body.grade;
	// tutor_req.phone = req.body.phone;
	// tutor_req.remark = req.body.remark;

	let ret = {};
	db.collection("tutor_request").insertOne(tutor_req)
		.then((result) => {
			

			transport.sendMail(mailOptions, (error, info) => {
    			if (error) {
        			console.log(error);
        			ret.rc = -1;
    				ret.error = error;
    			}
    			
    			ret.rc = 0;
				ret.insertedCount = result.insertedCount;
				res.send(ret);
			});
    	})
    	.catch((err) => {
    		ret.rc = -1;
    		ret.error = err;
    		res.send(ret);
    	});
    
};

exports.getTutorRequest = (req, res) => {	
	res.send('getTutorRequest');
};