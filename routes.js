import express from 'express';
import TutorCtrl from './TutorController'

let router = express.Router();

const setRoute = (app) => {
	router.get('/tutor', TutorCtrl.getTutorRequest);
	router.post('/tutor', TutorCtrl.createTutorRequest);

	app.use('/gem-mobile/api/v1', router);
};

export default { setRoute };